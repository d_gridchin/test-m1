<?php

$dir = str_replace(Engine::Get()->getProjectPath(), '', dirname(__FILE__));

Engine_HTMLHead::Get()->registerJsFile($dir.'/js/bootstrap.js');

Engine_HTMLHead::Get()->registerCssFile($dir.'/css/bootstrap.css');
Engine_HTMLHead::Get()->registerCssFile($dir.'/css/bootstrap-grid.css');
Engine_HTMLHead::Get()->registerCssFile($dir.'/css/bootstrap-reboot.css');
