<?php

/**
 * Class DBObject
 */
class DBUpdater {

    /**
     * @var array
     */
    public $_migrationsPathArray = [];

    /**
     * @var bool
     */
    public $_migrationsDefaultPath = false;

    /**
     * @param $path
     * @param bool $defaultPath
     */
    public function addMigrationsPath ($path, $defaultPath = false) {
        $this->_migrationsPathArray[] = $path;

        // Автоподгрузка X-Классов
        spl_autoload_register (function ($class) use ($path) {
            $path = $path.'/db/'.$class.'.class.php';
            if (file_exists($path)) {
                include_once $path.'';
            }
        });


        if ($defaultPath) {
            $this->_migrationsDefaultPath = $path;
        }
    }

    public function getMigrationsPathArray () {
        return $this->_migrationsPathArray;
    }

    /**
     * @param bool $hard
     */
    public function run ($hard = false) {
        // Получаем списко файлов
        foreach ($this->_migrationsPathArray as $index => $path) {
            $migrations = scandir($path);


            foreach ($migrations as $migration) {
                if ($migration == '..' || $migration == '.' || strpos($migration, '.php') === false) {
                    continue;
                }



                $xPath = Engine::Get()->getProjectPath().'/'.str_replace(Engine::Get()->getProjectPath(), '', $path).'/db/';
                if (!is_dir($xPath)) {
                    mkdir($xPath, 0755, true);
                }

                // Получаем список файлов уже обработанных
                $pPath = $path.'/.p';
                $p = [];
                if (file_exists($pPath)) {
                    $p = json_decode(file_get_contents($pPath), true);
                }



                // Получаем списко файлов, который мы уже обработали
                if (in_array($migration, $p)) {
                    if (!$hard) {
                        continue;
                    }

                }

                // Подключаем файл с миграцией
                include_once $path.'/'.$migration.'';

                // Создаем экземпляр сласса с миграцией
                $className = basename($migration, '.php');
                $x = new $className;
                if (method_exists($x, 'run')) {
                    // Выполняем миграцию
                    $result = $x->run();

                    // Если это конечный вариант, больше не трогаем эту миграцию
                    if ($result) {
                        if (!in_array($migration, $p)) {
                            $p[] = $migration;
                            file_put_contents($pPath, json_encode($p));
                        }
                    }
                }
            }
        }

    }

    /**
     * @param $name
     * @param bool $path
     * @throws Exception
     */
    public function createMigration ($name, $path = false) {
        // Название класса
        $fileAndClassName = 'migration_'.$name;

        if (!$path && !$this->_migrationsDefaultPath) {
            throw new Exception('no default path & custom path');
        }

        $migrationPath = $this->_migrationsDefaultPath.$fileAndClassName.'.php';
        if ($path) {
            if (!is_dir(Engine::Get()->getProjectPath().'/'.$path)) {
                mkdir(Engine::Get()->getProjectPath().'/'.$path, 0755, true);
            }

            $migrationPath = Engine::Get()->getProjectPath().$path.$fileAndClassName.'.php';
        }

        // Создаем миграцию
        file_put_contents($migrationPath, "<?php\n\nclass {$fileAndClassName} {\n\n\tpublic function run () {\n\n\t}\n}");
    }

    /**
     * @return DBUpdater|null
     */
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;

}