<?php

/**
 * Class DBObject
 */
class DBTable {

    private $_tableName = '';
    private $_xName = '';

    public function __construct($tableName, $xName = false) {
        if (Engine::Get()->getMode('log')) {
            print "start migration: {$tableName} \n";
        }
        if (!$xName) {
            $xName = 'X'.ucfirst($tableName);
        }
        $this->_xName = $xName;
        $this->_tableName = $tableName;
    }

    public function checkExist () {
        $r = DB::Get()->query('select 1 from `'.$this->_tableName.'`');
        if ($r) {
            return true;
        } else {
            return false;
        }
    }


    public function create () {
        if (!$this->checkExist()) {
            DB::Get()->query("CREATE TABLE IF NOT EXISTS `{$this->_tableName}` (`id` int(11) AUTO_INCREMENT PRIMARY KEY)");
            if (Engine::Get()->getMode('log')) {
                print "  table {$this->_tableName} has been created. \n";
            }
        } elseif (Engine::Get()->getMode('log')) {
            print "  table {$this->_tableName} exist. \n";
        }
    }

    public function save() {
        $trace = debug_backtrace();
        $path = dirname($trace[0]['file']);

        $result = DB::Get()->getConnect()->prepare("SELECT * FROM `{$this->_tableName}`");
        $result->execute();

        // Получаем кол-во колонок
        $columnCount = $result->columnCount();
        $allColumns = array();
        for ($i = 0; $i <= $columnCount - 1; $i++) {
            $column = $result->getColumnMeta($i);
            $allColumns[] = $column['name'];
        }

        foreach ($this->_fields as $field) {
            if (!$allColumns || !in_array($field['name'], $allColumns)) {
                $r = DB::Get()->getConnect()->prepare("ALTER TABLE `{$this->_tableName}` ADD {$field['name']} {$field['type']} NOT NULL");
            } else {
                if (isset($field['remove'])) {
                    $r = DB::Get()->getConnect()->prepare("ALTER TABLE `{$this->_tableName}` DROP COLUMN `{$field['name']}`");
                } else {
                    $r = DB::Get()->getConnect()->prepare("ALTER TABLE `{$this->_tableName}` CHANGE {$field['name']} {$field['name']} {$field['type']} NOT NULL");
                }
            }
            $r->execute();
        }

        foreach ($allColumns as $columnName) {
            if (!isset($this->_fields[$columnName])) {
                $this->_fields[$columnName] = array('name' => $columnName);
            } elseif (isset($this->_fields[$columnName]['remove'])) {
                unset($this->_fields[$columnName]);
            }
        }
        $this->createXFile($path);
    }

    private function createXFile ($path = false) {

        $dataToFile = array();
        $dataToFile[] = '<?php'."\n";
        $dataToFile[] .= "/**\n * class {$this->_xName}\n */\n";
        $dataToFile[] .= "class {$this->_xName} extends DBObject {\n\n";



        $fieldArray = array();
        foreach ($this->_fields as $field) {
            $fieldArray[] = "'".$field['name']."'";

            foreach (array('set', 'get') as $methodPrefix) {
                $methodName = $methodPrefix.ucfirst($field['name']);
                $columnName = lcfirst(str_replace("get", "", str_replace("set", "", $methodName)));

                if (strpos($methodName, 'set') !== false) {
                    $dataToFile[] .= "    /**\n     * @param \${$columnName}\n     * @return \$this\n     * @throws Exception\n     */\n";
                    $dataToFile[] .= "    public function {$methodName} ($$columnName) {return \$this->setField('$columnName', $$columnName);}\n\n";
                } elseif (strpos($methodName, 'get') !== false) {
                    $dataToFile[] .= "    /**\n     * Get {$columnName}\n     */\n";
                    $dataToFile[] .= "    public function {$methodName} () { return \$this->getField('{$columnName}');}\n\n";
                }
            }
        }


        $dataToFile[] .= "    /**\n";
        $dataToFile[] .= "     * Create an object\n";
        $dataToFile[] .= '     * @param int $id'."\n";
        $dataToFile[] .= "     */\n";
        $dataToFile[] .= '    public function __construct($id = 0) {'."\n";
        $dataToFile[] .= '        $this->setTablename(\''.$this->_tableName.'\');'."\n";
        $dataToFile[] .= '        $this->setClassname(__CLASS__);'."\n";
        $dataToFile[] .= '        parent::__construct($id);'."\n";
        $dataToFile[] .= '    }'."\n";
        $dataToFile[] .= "\n";
        $dataToFile[] .= '    /**'."\n";
        $dataToFile[] .= '     * @param bool $exception'."\n";
        $dataToFile[] .= '     * @return '.$this->_xName."\n";
        $dataToFile[] .= '     */'."\n";
        $dataToFile[] .= '    public function getNext($exception = false) {return parent::getNext($exception); }'."\n";
        $dataToFile[] .= "\n";
        $dataToFile[] .= '    /**'."\n";
        $dataToFile[] .= '     * @param $key'."\n";
        $dataToFile[] .= '     * @return '.$this->_xName."\n";
        $dataToFile[] .= '     */'."\n";
        $dataToFile[] .= '    public static function Get($key) {return self::GetObject("'.$this->_tableName.'", $key);}'."\n";
        $dataToFile[] .= "\n";
        $dataToFile[] .= "}\n";

        $dataToFile[] .= "\n";
        $dataToFile[] .= 'DBObject::SetFieldArray(\''.$this->_tableName.'\', array('.implode(', ', $fieldArray).'));'."\n";

        $data = implode("", $dataToFile);

            // $path


        // записываем класс в файл
        if ($path) {
            file_put_contents($path.'/db/'.$this->_xName.'.class.php', $data,LOCK_EX);
        } else {
            file_put_contents(Engine::Get()->getProjectPath().'/_api/db/'.$this->_xName.'.class.php', $data,LOCK_EX);
        }
    }


    private $_fields = array();
    public function addField ($name, $type, $additionalSettings = array()) {
        // Получаем все поля

        if (is_array($type)) {
            $type = "enum('".implode("', '", $type)."')";
        }
        $this->_fields[$name] = array('name' => $name, 'type' => $type, 'additionalSettings' => $additionalSettings);
    }

    public function removeField ($fieldName) {
        if (!isset($this->_fields[$fieldName])) {
            $this->_fields[$fieldName] = array('name' => $fieldName);
        }

        $this->_fields[$fieldName]['remove'] = true;
    }

    public function delete () {
        if ($this->checkExist()) {
            DB::Get()->query("DROP TABLE `{$this->_tableName}`");
            if (Engine::Get()->getMode('log')) {
                print "  table {$this->_tableName} has been removed. \n";
            }
        } else {
            print "  table {$this->_tableName} not exist\n";
        }
    }

}