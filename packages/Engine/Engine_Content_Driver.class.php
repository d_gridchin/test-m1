<?php

/**
 * Class Engine_Content_Driver
 */
class Engine_Content_Driver {

    private $_baseFolder = '/contents';
    private $_contents = array();
    public $_extendClass = 'Engine_Content';

    public function setBaseFolder ($folderPath) {
        $this->_baseFolder = $folderPath;
    }

    public function getBaseFolder () {
        return $this->_baseFolder;
    }

    public function registerContent ($contentId, $fieldsArray = array(), $prefix = false, $default = false) {
        $folder = $this->_baseFolder;

        // Префикс
        if ($prefix) {
            $fieldsArray['prefix'] = $prefix;
        }

        $fieldsArray['id'] = $contentId;

        if (isset($fieldsArray['admin']) && $fieldsArray['admin']) {
            $folder = str_replace('//', '/', $folder.'/admin/');
        }

        if (isset($fieldsArray['php']) && $fieldsArray['php']) {
            $fieldsArray['php'] = str_replace("//", "/", str_replace("//" ,"/" ,$folder.'/'.$fieldsArray['php'].'.php'));
        }

        if (isset($fieldsArray['html']) && $fieldsArray['html']) {
            $fieldsArray['html'] = str_replace("//", "/", str_replace("//" ,"/" ,$folder.'/'.$fieldsArray['html'].'.phtml'));
        }


        $this->_contents[$contentId] = $fieldsArray;
        if ($default) {
            define('default_content', $contentId);
            Engine::Get()->setConfigField('default_content', $contentId);
        }
    }

    public function synchronizationContents () {
        $contents = (array)$this->_contents;
        foreach ($contents as $cid => $fieldsArray) {
            if (isset($fieldsArray['php']) && $fieldsArray['php']) {
                $dir = str_replace('//', '/', Engine::Get()->getProjectPath().'/'.substr($fieldsArray['php'], 0, strrpos($fieldsArray['php'], '/') + 1));

                if (!is_dir($dir)) {
                    mkdir($dir, 0755, true);
                }

                $file = str_replace('//', '//', Engine::Get()->getProjectPath().'/'.$fieldsArray['php']);
                if (!file_exists($file)) {
                    $classDataArray = [
                        '<?php',
                        "\n\n",
                        "class ".basename($fieldsArray['php'], '.php')." extends ".$this->_extendClass." {",
                        "\n\n",
                        "    public function process() {\n\n    }\n\n}"
                    ];
                    file_put_contents($file, implode("", $classDataArray));
                }
            }

            if (isset($fieldsArray['html']) && $fieldsArray['html']) {
                $dir = str_replace('//', '/', Engine::Get()->getProjectPath().'/'.substr($fieldsArray['html'], 0, strrpos($fieldsArray['html'], '/') + 1));
                if (!is_dir($dir)) {
                    mkdir($dir, 0755, true);
                }
                $file = str_replace('//', '//', Engine::Get()->getProjectPath().'/'.$fieldsArray['html']);
                if (!file_exists($file)) {
                    file_put_contents($file, '');
                }
            }
        }
    }

    /**
     * @param bool $url
     * @return bool
     */
    public function getContent ($url = false) {
        if ($url === false) {
            $url = @reset(explode('?', $_SERVER["REQUEST_URI"]));
        }
        $this->_contents = array_reverse($this->_contents);

        foreach ($this->_contents as $contentId => $content) {
            $uri = @$content['url'];
            if (!$url) {
                continue;
            }

            if (!is_array($uri)) {
                $uri = (array) $uri;
            }

            foreach ($uri as $u) {
                if ($u == $url) {
                    return $this->_contents[$contentId];
                }
                

                $urlArray = explode('/', $url);
                $uArray = explode('/', $u);

                if (count($urlArray) == count($uArray)) {
                    $found = true;
                    $params = array();
                    foreach ($uArray as $k => $v) {
                        if ((strpos($v, '{') !== false) && (strpos($v, '}') !== false)) {
                            $params[str_replace('{', '', str_replace('}', '', $v))] = $urlArray[$k];
                            continue;
                        }
                        if ($urlArray[$k] != $uArray[$k]) {
                            $found = false;
                            break;
                        }
                    }
                    if ($found) {
                        // Нашли то что нада!!!
                        $this->_contents[$contentId]['params'] = $params;
                        return $this->_contents[$contentId];
                    }

                }
            }
        }
        return false;
    }


    /**
     * @param $contentId
     * @return bool
     */
    public function getContentById ($contentId) {
        if (isset($this->_contents[$contentId])) {
            return $this->_contents[$contentId];
        }
        return false;
    }

    public function contentExist ($contentId) {
        if ($this->getContentById($contentId)) {
            return true;
        }

        return false;
    }

    /**
     * @return Engine_Content_Driver|null
     */
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;

}