<?php

spl_autoload_register (function ($class) {
    $path = dirname(__FILE__)."/{$class}.class.php";
    if (file_exists($path)) {
        include_once $path.'';
    }

    // Подключаем сервисы
    if (strpos($class, '_Service') !== false) {
        $path = realpath(dirname(__FILE__).'/../../_api/services/');
        if (file_exists($path.'/'.$class.'.class.php')) {
            include_once $path.'/'.$class.'.class.php';
        }
    }
});

include_once (dirname(__FILE__).'/../PackageLoader/include.php');
