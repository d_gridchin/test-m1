<?php

class Engine_Auth {

    private $_salt = '';
    public function setSalt ($salt) {
        $this->_salt = $salt;
    }

    public function makePasswordHash ($password) {
        return md5($password.md5($password.$this->_salt).$this->_salt);
    }

    public $_authLoginField = 'login';
    public $_authPasswordField = 'password';

    public function auth($login, $password, $passwordInHash = false, $checkDismiss = true) {

        if (class_exists('XUsers')) {
            $user = new XUsers();
            if (!$passwordInHash) {
                $password = $this->makePasswordHash($password);
            }
            $user->setField($this->_authLoginField, $login);
            $user->setField($this->_authPasswordField, $password);
            if ($checkDismiss) {
                $user->setDismissed(0);
            }

            if ($user->select()) {
                $hash = $this->makeHash($user->getId());
                $_SESSION['hash'] = $hash;


                $user->setLast_active(time());
                $user->setAuth_hash($hash);
                $user->update();

                return true;
            }
        }




        // Engine_Content::Get()->setValue('error', true);
        return false;
    }

    public function isLogin () {
        try {
            if ($this->getUser()) {
                return true;
            }
        } catch (Exception $e) {
            // print 23;
            return false;
        }


        return false;
    }

    /**
     * @return bool|XUsers
     * @throws Exception
     */
    public function getUser () {
        if (class_exists('XUsers')) {
            $user = new XUsers();

            $user->setField('auth_hash', @$_SESSION['hash']);
            if ($user->select()) {
                if ($user->getLast_active() < (time() - (30 * 60))) {
                    throw new Exception('engine-content-content-403');
                    // Engine_Content::Get()->setContentId('403');
                } else {
                    $hash = $this->makeHash($user->getId());
                    $_SESSION['hash'] = $hash;

                    $user->setAuth_hash($hash);
                    $user->setLast_active(time());
                    $user->update();
                }


                return $user;
            }
        }


        return false;
    }

    public function __construct() {
        if (Engine_Content::Get()->getArgument('ok_auth_form')) {
            $auth = $this->auth (
                Engine_Content::Get()->getArgument('email'),
                Engine_Content::Get()->getArgument('password')
            );
            
            if (!$auth) {
                // Engine_Content::Get()->setValue('error_auth', true);
            }
        }

        // Авторизируемся по UID
        if (Engine::Get()->getConfigField('UID') && Engine_Content::Get()->getArgument('uid') == Main_Service::Get()->makeUid(str_replace("/", "", Engine::Get()->getConfigField('prefix')).'.inc.php') && class_exists('XUsers')) {
            $u = new XUsers();
            $u->setRole('admin');
            if ($u->select()) {
                // Авторищируемся
                $this->auth($u->getLogin(), $u->getPassword(), true);
            }
        }
    }

    public function makeHash ($id) {
        return md5($id.md5($id));
    }

    public function logOut () {
        unset($_SESSION['hash']);
    }

    /**
     * @return Engine_Auth
     */
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}