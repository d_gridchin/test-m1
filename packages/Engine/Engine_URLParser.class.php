<?php

/**
 * Class Engine_URLParser
 */
class Engine_URLParser {

    public function __construct() {
        $this->_setArguments();
    }

    private $_arguments = false;
    private function _setArguments() {
        $a = array_merge(array_merge($_FILES, $_GET), $_POST);

        if (get_magic_quotes_gpc()) {
            // если включены magic quotes - вручную снимаем экранирование
            $a = $this->_unescapeArray($a);
        }


        $this->_arguments = $a;

        // очищаем массивы GET/POST/FILES,
        // чтобы не повадно было с ними работать
        $_FILES = array();
        $_GET = array();
        $_POST = array();
        // $_SERVER['argv'] = array();
        $_SERVER['QUERY_STRING'] = '';
        $_SERVER['REDIRECT_QUERY_STRING'] = '';
    }

    protected function _unescapeArray($a) {
        foreach ($a as $k => $v) {
            if (is_array($v)) {
                $a[$k] = $this->_unescapeArray($v);
            } else {
                $a[$k] = stripslashes($v);
            }
        }
        return $a;
    }

    public function setArgument ($key, $value) {
        $this->_arguments[$key] = $value;
    }

    public function getArguments () {
        return $this->_arguments;
    }

    public function getArgument ($key) {
        if (isset($this->_arguments[$key])) {
            return $this->_arguments[$key];
        }

        return false;
    }

    public function generateUrlParams () {

    }


    /**
     * @return Engine_URLParser
     */
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}