<?php

/**
 * Class Engine
 */
class Engine {

    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    public function getProjectPath () {
        return dirname(dirname(dirname(__FILE__)));
    }

    public function errorReporting () {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
    }

    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    private $_modeArray = array();
    public function setMode ($mode) {
        if ($mode == 'errorReporting') {
            $this->errorReporting();
        }
        $this->_modeArray[] = $mode;
    }

    public function getMode ($mode) {
        return in_array($mode, $this->_modeArray);
    }

    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    public function __construct() {

    }

    public function init () {
        PackageLoader::Get()->loadPackage('DB');
        PackageLoader::Get()->loadPackage('Observer');

        $configFileName = '/config.php';
        include_once $this->getProjectPath().$configFileName.'';

        // Обсервер после подключения файла config.php
        Observer::Get()->observe('afterConfig');

        $includeFileName = '/_api/include.php';
        include_once $this->getProjectPath().$includeFileName.'';

        // Обсервер после инициализации движка
        Observer::Get()->observe('afterEngineInit');
    }

    public function getContentId () {
        return $this->_currentContentId;
    }

    public function getContentPrefix () {
        return $this->_currentContentPrefix;
    }




    public $_currentContentId = false;
    public $_currentContentPrefix = false;
    private $to = false;
    public function execute ($contentId = false) {
        try {
            if ($contentId) {
                $content = Engine_Content_Driver::Get()->getContentById($contentId);
            } else {
                $content = Engine_Content_Driver::Get()->getContent();
                if (!$content) {
                    $content = Engine_Content_Driver::Get()->getContentById('404');
                }
            }
            if (isset($content['url'])) {
                $this->_currentContentId = $content['id'];
                if (isset($content['prefix'])) {
                    $this->_currentContentPrefix = $content['prefix'];
                }
            }

            // Генерируем событие
            Observer::Get()->observe('beforeEngineRender');

            // Сначала нужно узнать кол-во вложений
            $rc = $content;
            $recursiveCnt = 0;
            while (true) {
                if (isset($rc['parent_content']) && $rc['parent_content']) {
                    $recursiveCnt++;
                    $rc = Engine_Content_Driver::Get()->getContentById($rc['parent_content']);
                } else {
                    break;
                }
            }
            Engine_HTMLHead::Get()->_file_index = ($recursiveCnt + 1);

            // RENDER
            while (true) {
                if (!empty($content) && $content) {
                    if (@$content['php']) {


                        include_once $this->getProjectPath()."/".$content['php'].'';

                        if (isset($content['params']) && $content['params']) {
                            foreach ($content['params'] as $k => $v) {
                                Engine_URLParser::Get()->setArgument($k, $v);
                            }
                        }

                        $class = basename($content['php'], '.php');

                        // правильная сортировка JS / CSS Файлов
                        if (isset($content['title']) && $content['title']) {
                            Engine_HTMLHead::Get()->setTitle($content['title']);
                        }
                        Engine_HTMLHead::Get()->setSort($content['id']);

                        $x = new $class;
                        $res = $x->process();
                        if ($res) {
                            return $res;
                        }
                    }

                    if (@$content['html']) {
                        $html = $content['html'];
                        if ($this->getMode('dev')) {
                            if (file_exists($this->getProjectPath().str_replace('.phtml', '.dev.phtml', $content['html']))) {
                                $html = str_replace('.phtml', '.dev.phtml', $content['html']);
                            }
                        }
                        Template::Get()->setTemplatePath($html);
                    }

                    Engine_HTMLHead::Get()->_file_index--;

                    if (@$content['parent_content']) {
                        if (@$content['html']) {
                            $this->to = @$content['to'] ? $content['to'] : 'content';

                            $c = Template::Get()->render();
                            Template::Get()->set($this->to, $c);
                        }

                        $content = Engine_Content_Driver::Get()->getContentById($content['parent_content']);
                    } else {
                        if (@$content['html']) {
                            Template::Get()->display();
                        }
                        break;
                    }

                } else {
                    break;
                }
            }

            Observer::Get()->observe('afterEngineRender');
        } catch (Exception $e) {
            if ($this->getMode('log') && strpos($e->getMessage(), 'engine-content-content-') === false) {
                Engine_Log::Get()->log($e->getMessage());
            }

            if (strpos($e->getMessage(), 'engine-content-content-') !== false) {
                $this->execute(str_replace('engine-content-content-', '', $e->getMessage()));
            }
        }
        return true;
    }

    private $_configFieldsArray = array();
    public function setConfigField ($field, $value) {
        $this->_configFieldsArray[$field] = $value;
    }

    public function getConfigField ($field) {
        if (isset($this->_configFieldsArray[$field])) {
            return $this->_configFieldsArray[$field];
        }
        return false;
    }


    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}