<?php

session_start();

// Отключаем показ ошибок PHP (Ворнинги и нотисы)
ini_set('error_reporting', 0);
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);

include_once (dirname(__FILE__).'/include.php');

// Инициализируем движек
Engine::Get()->init();

// Стартуем
Engine::Get()->execute();