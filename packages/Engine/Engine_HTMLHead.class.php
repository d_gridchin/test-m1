<?php

/**
 * Class Engine_HTMLHead
 */
class Engine_HTMLHead {

    private $_sort = array();

    public $_file_index = 0;
    public $_file_array = [];

    // Данная штука будет применятся только для контентов
    public function setSort ($sort) {
        array_unshift($this->_sort, $sort);
    }


    private $_filesArray = array();
    public function registerCssFile ($filePath) {
        $this->registerFile('css', $filePath);
    }

    public function registerJsFile ($filePath) {
        $this->registerFile('js', $filePath);
    }

    private function registerFile ($type, $filePath) {
        $filePath = str_replace('//', '/', $filePath);
        $this->_file_array[$this->_file_index][$type][] = $filePath;
    }

    /**
     * @param $fileType
     * @return mixed
     */
    public function getFiles ($fileType) {
        return $this->_filesArray[$fileType];
    }

    private $_title = '';
    public function setTitle ($title) {
        $this->_title = $title;
    }

    public function getTitle () {
        return $this->_title;
    }

    public function getTitleSeparator () {
        return ' / ';
    }

    public function getVersion () {
        return time();
    }

    public function makeHtmlHead ($data = 'all') {
        $t = new Template();
        $t->setTemplatePath(dirname(__FILE__).'/Engine_HTMLHead.phtml');
        if ((is_array($data) && in_array('title', $data)) || ($data == 'title' || $data == 'all')) {
            $t->set('title', $this->_title);
        }

        // Сортируем файлы (Как нам нужно)
        ksort($this->_file_array);

        $postFix = true;

        $filesArray = [];
        foreach ($this->_file_array as $index => $files) {
            foreach ($files as $fileType => $file) {
                if (is_array($data) && in_array($fileType, $data) || ($data == $fileType || $data == 'all')) {
                    if (!isset($filesArray[$fileType]) || !$filesArray[$fileType]) {
                        $filesArray[$fileType] = [];
                    }
                    foreach ($file as $f) {
                        $fPath = $f;
                        if ($postFix) {
                            $fPath .= '?dv='.time();
                        }
                        $filesArray[$fileType][] = $fPath;
                    }
                }
            }
        }
        foreach ($filesArray as $k => $v) {
            $t->set($k.'Files', $v);
        }


//        foreach (['css', 'js'] as $fileType) {
//            if (isset($this->_filesArray[$fileType]) && $this->_filesArray[$fileType]) {
//                ksort($this->_filesArray[$fileType]);
//                array_walk($this->_filesArray[$fileType], function (&$filePath) {
//                    $filePath .= '?v='.$this->getVersion();
//                });
//
//

//
//            }
//        }
        return $t->render();
    }


    /**
     * @return Engine_HTMLHead
     */
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}