<?php

/**
 * Class PackageLoader
 */
class PackageLoader {
    public function loadPackage ($packageName) {
        include_once realpath(Engine::Get()->getProjectPath()."/packages/{$packageName}/include.php").'';
    }

    private $_loadedModules = [];

    public function loadModule ($moduleName) {
        $this->_loadedModules[] = $moduleName;
        include_once realpath(Engine::Get()->getProjectPath()."/_api/_modules/{$moduleName}/include.php").'';
    }

    public function isLoadedModule ($moduleName) {
        return in_array($moduleName, $this->_loadedModules);
    }

    public function loadAllModules () {
        $modulesPath = Engine::Get()->getProjectPath().'/_api/_modules/';
        $modules = scandir($modulesPath);
        foreach ($modules as $module) {
            $i = $modulesPath.'/'.$module.'/include.php';
            if ($module != '.' && $module != '..' && file_exists($i)) {
                include_once $i.'';
            }
        }
    }



//    private function preLoad () {
//        if (Engine::Get()->getMode('development')) {
//            PackageLoader::Get()->loadPackage('VersionControl');
//        }
//
//
//    }
//
//    /** Пакеты **/
//    private $_loadPackagesArray = array();
//    public function loadPackage ($packageName) {
//        $this->_loadPackagesArray[] = $packageName;
//        include_once (Engine::Get()->getProjectPath().'/packages/'.$packageName.'/include.php');
//    }
//
//
//    /** Helpers **/
//    private $_loadHelpersArray = array();
//    public function loadHelper ($helperName) {
//        $this->_loadHelpersArray[] = $helperName;
//        include_once (Engine::Get()->getProjectPath().'/_api/helpers/'.$helperName.'/include.php');
//    }
//
//
//
//
//    /** Модули **/
//    private $_loadModulesArray = array();
//    public function loadModules () {
//        foreach ($this->_loadModulesArray as $moduleName) {
//            include_once (Engine::Get()->getProjectPath().'/_config/modules/'.$moduleName.'/include.php');
//        }
//    }
//
//    public function registrationModule ($moduleName) {
//        $this->_loadModulesArray[] = $moduleName;
//    }
//
//    public function isModuleLoaded ($moduleName) {
//        return in_array($moduleName, $this->_loadModulesArray);
//    }
//
//
//
//    private $_phpDirectory = array();
//    public function registerPhpDirectory ($directoryName, $extension = '.class.php') {
//        $this->_phpDirectory[] = array('directoryName' => $directoryName, 'extension' => $extension);
//    }
//
//    public function getPhpDirectory () {
//        return $this->_phpDirectory;
//    }
//
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}