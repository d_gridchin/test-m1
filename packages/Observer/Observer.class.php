<?php

class Observer {

    private $_eventsArray = array();
    public function addEvent ($eventName, $class, $methodName = 'notify', $params = array()) {
        if (!isset($this->_eventsArray[$eventName])) {
            $this->_eventsArray[$eventName] = array();
        }
        $this->_eventsArray[$eventName][] = array('class' => $class, 'method' => $methodName, 'params' => $params);

    }

    public function observe ($eventName) {
        if (isset($this->_eventsArray[$eventName])) {
            foreach ($this->_eventsArray[$eventName] as $item) {
                $class = $item['class'];
                $methodName = $item['method'];
                $params = $item['params'];

                $class->$methodName($params);
            }
        } else {
            
        }
    }



    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}