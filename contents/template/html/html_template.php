<?php

class html_template extends Engine_Content {

    public function process() {
        Engine_HTMLHead::Get()->registerJsFile('/_js/loading.js');
        Engine_HTMLHead::Get()->registerJsFile('/_js/bootstrap_modal.js');
    }

}