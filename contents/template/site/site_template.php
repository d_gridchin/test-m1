<?php

class site_template extends Engine_Content {

    public function process() {
        // Основная информация про библиотеку
        $info = Card_Service::Get()->getMainInformation();
        $this->setValue('all_card', $info['cnt']);
        $this->setValue('all_duration', $info['all_duration_hours_round']);
        $this->setValue('all_price', $info['all_price']);


        Engine_HTMLHead::Get()->registerCssFile('/_css/site.css');
        Engine_HTMLHead::Get()->registerJsFile('/_js/site.js');
    }

}