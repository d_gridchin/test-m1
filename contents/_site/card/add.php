<?php

class add extends Engine_Content {

    public function process() {
        $resultArray = [];
        $errorArray = [];

        $name = $this->getArgument('name');
        $artistId = $this->getArgument('artist_id');
        $artistName = $this->getArgument('artist_name');
        $year = $this->getArgument('year');
        $price = $this->getArgument('price');

        // Сразу переводим продолжительность в секунды
        $duration = ($this->getArgument('duration_hour') * 60 * 60) + ($this->getArgument('duration_minute') * 60);

        $addDateMonth = $this->getArgument('add_date_month');
        if ($addDateMonth < 10) {
            $addDateMonth = '0'.$addDateMonth;
        }
        $addDateDay = $this->getArgument('add_date_day');
        if ($addDateDay < 10) {
            $addDateDay = '0'.$addDateDay;
        }

        $addDate = $this->getArgument('add_date_year').'-'.$addDateMonth.'-'.$addDateDay;
        $code = $this->getArgument('code_room').':'.$this->getArgument('code_unit_box').':'.$this->getArgument('code_unit');

        $image = $this->getArgument('image');

        // Проверяем на ошибки
        if (!$name) $errorArray[] = 'name';
        if ($artistId == '-1' && !$artistName) $errorArray[] = 'artist';
        if (!$duration) $errorArray[] = 'duration';
        if ($addDate > date('Y-m-d')) $errorArray[] = 'add_date_incorrect';
        if (!$price || $price < 0) $errorArray[] = 'price';

        $card = new XCard();
        $card->setStorage_code($code);
        if ($card->select()) {
            $errorArray[] = 'code';
        }

        if ($errorArray) {
            $resultArray['status'] = 'error';
            $resultArray['errorArray'] = $errorArray;
        } else {
            $card = new XCard();
            $card->setName($name);
            $card->setYear($year);
            $card->setDuration($duration);
            $card->setAdd_date($addDate);
            $card->setPrice($price);
            $card->setStorage_code($code);

            if ($artistId == '-1') {
                $artist = new XArtist();
                $artist->setName($artistName);
                if (!$artist->select()) {
                    $artistId = $artist->insert();
                } else {
                    $artistId = $artist->getI;
                }
            }

            $card->setArtist_id($artistId);


            if ($image && is_array($image) && isset($image['tmp_name']) && $image['tmp_name']) {
                $imagePath = '/media/'.time().'_'.rand(1000, 9999).'.'.pathinfo($image['name'], PATHINFO_EXTENSION);

                if (copy($image['tmp_name'], Engine::Get()->getProjectPath().$imagePath)) {
                    $card->setImg_path($imagePath);
                }
            }

            $card->insert();

            $resultArray['status'] = 'ok';
        }

        echo json_encode($resultArray);
    }

}