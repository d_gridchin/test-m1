<?php

class get_list extends Engine_Content {

    public function process() {
        $artist = new XArtist();
        $artistArray = [];

        while ($x = $artist->getNext()) {
            $artistArray[] = ['id' => $x->getId(), 'name' => $x->getName()];
        }

        echo json_encode($artistArray);
        exit(0);
    }

}