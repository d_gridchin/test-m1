<?php

include_once dirname(__FILE__).'/packages/Engine/include.php';
Engine::Get()->init();

// Название миграции
$migrationName = time();
if (isset($argv[1]) && $argv[1]) {
    $migrationName = $argv[1];
}

$migrationPath = false;
if (isset($argv[2]) && $argv[2]) {
    $path = $argv[2];

    if (strpos($path, 'module/') !== false) {
        $path = "/_api/_modules/".str_replace("module/", "", $path).'/migrations/';
    }
    $migrationPath = $path;
}


try {
    DBUpdater::Get()->createMigration($migrationName, $migrationPath);
} catch (Exception $e) {
    print $e->getMessage();
}
