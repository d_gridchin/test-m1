<?php
/**
 * class XArtist
 */
class XArtist extends DBObject {

    /**
     * @param $name
     * @return $this
     * @throws Exception
     */
    public function setName ($name) {return $this->setField('name', $name);}

    /**
     * Get name
     */
    public function getName () { return $this->getField('name');}

    /**
     * @param $id
     * @return $this
     * @throws Exception
     */
    public function setId ($id) {return $this->setField('id', $id);}

    /**
     * Get id
     */
    public function getId () { return $this->getField('id');}

    /**
     * Create an object
     * @param int $id
     */
    public function __construct($id = 0) {
        $this->setTablename('artist');
        $this->setClassname(__CLASS__);
        parent::__construct($id);
    }

    /**
     * @param bool $exception
     * @return XArtist
     */
    public function getNext($exception = false) {return parent::getNext($exception); }

    /**
     * @param $key
     * @return XArtist
     */
    public static function Get($key) {return self::GetObject("artist", $key);}

}

DBObject::SetFieldArray('artist', array('name', 'id'));
