<?php
/**
 * class XCard
 */
class XCard extends DBObject {

    /**
     * @param $img_path
     * @return $this
     * @throws Exception
     */
    public function setImg_path ($img_path) {return $this->setField('img_path', $img_path);}

    /**
     * Get img_path
     */
    public function getImg_path () { return $this->getField('img_path');}

    /**
     * @param $name
     * @return $this
     * @throws Exception
     */
    public function setName ($name) {return $this->setField('name', $name);}

    /**
     * Get name
     */
    public function getName () { return $this->getField('name');}

    /**
     * @param $artist_id
     * @return $this
     * @throws Exception
     */
    public function setArtist_id ($artist_id) {return $this->setField('artist_id', $artist_id);}

    /**
     * Get artist_id
     */
    public function getArtist_id () { return $this->getField('artist_id');}

    /**
     * @param $year
     * @return $this
     * @throws Exception
     */
    public function setYear ($year) {return $this->setField('year', $year);}

    /**
     * Get year
     */
    public function getYear () { return $this->getField('year');}

    /**
     * @param $duration
     * @return $this
     * @throws Exception
     */
    public function setDuration ($duration) {return $this->setField('duration', $duration);}

    /**
     * Get duration
     */
    public function getDuration () { return $this->getField('duration');}

    /**
     * @param $add_date
     * @return $this
     * @throws Exception
     */
    public function setAdd_date ($add_date) {return $this->setField('add_date', $add_date);}

    /**
     * Get add_date
     */
    public function getAdd_date () { return $this->getField('add_date');}

    /**
     * @param $price
     * @return $this
     * @throws Exception
     */
    public function setPrice ($price) {return $this->setField('price', $price);}

    /**
     * Get price
     */
    public function getPrice () { return $this->getField('price');}

    /**
     * @param $storage_code
     * @return $this
     * @throws Exception
     */
    public function setStorage_code ($storage_code) {return $this->setField('storage_code', $storage_code);}

    /**
     * Get storage_code
     */
    public function getStorage_code () { return $this->getField('storage_code');}

    /**
     * @param $id
     * @return $this
     * @throws Exception
     */
    public function setId ($id) {return $this->setField('id', $id);}

    /**
     * Get id
     */
    public function getId () { return $this->getField('id');}

    /**
     * Create an object
     * @param int $id
     */
    public function __construct($id = 0) {
        $this->setTablename('card');
        $this->setClassname(__CLASS__);
        parent::__construct($id);
    }

    /**
     * @param bool $exception
     * @return XCard
     */
    public function getNext($exception = false) {return parent::getNext($exception); }

    /**
     * @param $key
     * @return XCard
     */
    public static function Get($key) {return self::GetObject("card", $key);}

}

DBObject::SetFieldArray('card', array('img_path', 'name', 'artist_id', 'year', 'duration', 'add_date', 'price', 'storage_code', 'id'));
