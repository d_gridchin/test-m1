<?php

class migration_cards {

	public function run () {
        $table = new DBTable('card', 'XCard');
        $table->create();

        $table->addField('img_path', 'varchar(255)');
        $table->addField('name', 'varchar(255)');
        $table->addField('artist_id', 'int(11)');
        $table->addField('year', 'int(4)');
        $table->addField('year', 'int(4)');
        $table->addField('duration', 'int(11)');
        $table->addField('add_date', 'date');
        $table->addField('price', 'decimal(10,2)');
        $table->addField('storage_code', 'varchar(50)');
        $table->save();
	}
}