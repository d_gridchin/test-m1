<?php

class migration_artist {

	public function run () {
        $table = new DBTable('artist', 'XArtist');
        $table->create();

        $table->addField('name', 'varchar(255)');
        $table->save();
	}
}