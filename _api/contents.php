<?php

/** --- --- --- --- --- --- --- --- Templates --- --- --- --- --- --- --- --- **/
Engine_Content_Driver::Get()->registerContent('html-template', [
    'html' => '/template/html/html_template',
    'php' => '/template/html/html_template',
], false, true);


Engine_Content_Driver::Get()->registerContent('site-template', [
    'html' => '/template/site/site_template',
    'php' => '/template/site/site_template',
    'parent_content' => 'html-template'
]);

/** --- --- --- --- --- --- --- --- Site Pages --- --- --- --- --- --- --- --- **/
Engine_Content_Driver::Get()->registerContent('site-index', [
    'title' => 'Хранилище CD дисков',
    'url' => '/',
    'html' => '/_site/index/index',
    'php' => '/_site/index/index',
    'parent_content' => 'site-template'
]);

Engine_Content_Driver::Get()->registerContent('get-artist-list', [
    'url' => '/get-artist-list',
    'php' => '/_site/artist/get_list',
]);

Engine_Content_Driver::Get()->registerContent('card-add', [
    'url' => '/card-add',
    'php' => '/_site/card/add',
]);