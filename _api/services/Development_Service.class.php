<?php

class Development_Service {

    public function __construct() {

    }

    public function update ($print = true) {
        if ($print) {
            print "Update database".br;
        }
        DBUpdater::Get()->run();
        if ($print) {
            print "Database updates".br.br;
        }


        if ($print) {
            print "Generate empty contents".br;
        }
        Engine_Content_Driver::Get()->synchronizationContents();

        print "Done".br;
    }


    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }
    private static $_Instance = null;

}