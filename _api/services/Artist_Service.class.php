<?php

class Artist_Service {

    public function __construct() {

    }

    /**
     * Получить данные артиста по ID
     *
     * @param $artistId
     * @return array|bool
     * @throws Exception
     */
    public function getArtistById ($artistId) {
        $artist = new XArtist();
        $artist->setId($artistId);

        if ($artist->select()) {
            return $artist->toArray();
        }

        return false;
    }


    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }
    private static $_Instance = null;

}