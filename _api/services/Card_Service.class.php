<?php

class Card_Service {

    public function __construct() {

    }

    public function getCountOfCard () {
        $info = $this->getMainInformation();
        if ($info['cnt']) {
            return $info['cnt'];
        }
        return false;
    }

    private $_information = [];
    public function getMainInformation () {
        if (!$this->_information) {
            $this->_information['cnt'] = 0;
            $this->_information['all_duration'] = 0;
            $this->_information['all_price'] = 0;


            $card = new XCard();
            $r = DB::Get()->query("SELECT count(`id`) as cnt, sum(`price`) as all_price, sum(`duration`) as all_duration FROM `{$card->getTablename()}` WHERE 1");
            if ($r) {
                $r = $r->fetch(PDO::FETCH_ASSOC);

                $this->_information['cnt'] = $r['cnt'];
                $this->_information['all_duration_s'] = $r['all_duration'];
                $this->_information['all_duration_hours_round'] = $this->secondsToHoursRound($r['all_duration']);
                $this->_information['all_price'] = $r['all_price'];
            }

        }

        return $this->_information;
    }

    public function getCards ($from, $limit = 30) {
        $card = new XCard();

        $cardsArray = [];
        while ($x = $card->getNext()) {
            $img = false;
            if ($x->getImg_path()) {
                // TODO Нужно сделать все изображения 1 размера
                $img = $x->getImg_path();
            }

            $cardsArray[] = [
                'img' => $img,
                'first_latter' => mb_substr($x->getName(), 0, 1, "UTF-8"),
                'name' => $x->getName(),
                'artist' => Artist_Service::Get()->getArtistById($x->getArtist_id()),
                'year' => $x->getYear(),
                'duration' => $this->secondsToFormatted($x->getDuration()),
                'add_date' => date('m.d.Y', strtotime($x->getAdd_date())),
                'price' => $x->getPrice(),
                'code' => $x->getStorage_code()
            ];
        }

        return $cardsArray;
    }

    private function secondsToFormatted ($second) {
        $sec = $second % 60;
        $second = floor($second / 60);
        $min = $second % 60;
        $second = floor($second / 60);

        return $second.':'.$min;
    }

    private function secondsToHoursRound ($second) {
        return floor(($second / 3600));
    }


    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }
    private static $_Instance = null;

}