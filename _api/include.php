<?php

PackageLoader::Get()->loadPackage('SynchronizationProcessor');


// Загружаем нужные нам пакеты
PackageLoader::Get()->loadPackage('Template');
PackageLoader::Get()->loadPackage('jQuery');
PackageLoader::Get()->loadPackage('Observer');
PackageLoader::Get()->loadPackage('Bootstrap');


// Зашли через консоль
$cli = php_sapi_name() == 'cli';
Engine::Get()->setConfigField('cli', $cli);
if ($cli) {
    define('br', "\n");
} else {
    define('br', "<br>");
}

// Все контенты
Engine_Content_Driver::Get()->setBaseFolder('/contents/');

// Миграции
DBUpdater::Get()->addMigrationsPath(dirname(__FILE__).'/migrations/', true);


include_once dirname(__FILE__).'/contents.php';


// Если нужно обновить все через браузер
if (Engine::Get()->getMode('development')) {
    // Для начала нужно подключить все модули
    PackageLoader::Get()->loadAllModules();

    // Обновлям базу данных, и создаем контенты
    Development_Service::Get()->update();

    exit(br.br."<strong>Программа не работает в режиме development. Этот режим только для обновления базы данных, и созания пустых контентов.</strong>");
}
