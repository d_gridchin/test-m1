
// Модальное окно
var loading = loading || (function ($) {
    'use strict';

    var loading_dialog = $('<div/>')
        .css({
            'position': 'fixed',
            'width': '100%',
            'height': '100vh',
            'top': '0',
            'left': '0',
            'display': 'none',
            'background': 'rgba(0, 0, 0, 0.8) url("/_img/ball.svg") center center no-repeat',
            'z-index': '100500'
        });
    $('body').append(loading_dialog);

    return {

        show: function () {
            loading_dialog.fadeIn(100);

            return loading;
        },
        hide: function () {
            loading_dialog.fadeOut(100);
            return loading;
        }
    };
})(jQuery);