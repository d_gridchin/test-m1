// Модальное окно
var bootstrap_modal = bootstrap_modal || (function ($) {
    'use strict';

    var error_block = $('<div/>')
        .addClass('modal-body')
        .css('display', 'none')
        .css('padding-bottom', '0')
        .append(
            $('<div/>')
                .append(
                    $('<h3/>')
                        .css({
                            'font-weight': '100'
                        })
                        .text('Error')
                )
                .addClass('alert alert-warning')
                .css('margin', '0')
                .append(
                    $('<ul/>')

                )
        )

    var dialog = $('<div/>')
        .addClass("modal fade")
        .append(
            $('<div/>')
                .addClass('modal-dialog')
                .append(
                    $('<div/>').addClass('modal-content')
                        .append(
                            $('<div/>')
                                .addClass('modal-header')
                                .append(
                                    $('<h4/>').addClass('modal-title')
                                )
                        )
                        .append(
                            error_block
                        )
                        .append(
                            $('<div/>').addClass('main-content')
                        )
                )
        );
    $('body').append(dialog);

    dialog.on('hidden.bs.modal', function () {
        dialog.find('.modal-dialog').removeClass('modal-lg');
        dialog.find('.modal-dialog').removeClass('modal-sm');
    })

    return {
        getModal: function () {
            return dialog;
        },

        showErrorBlock: function () {
            error_block.show();

            return bootstrap_modal;
        },
        addError: function (errorMessage) {
            error_block.find('ul')
                .css({
                    'margin': '0',
                    'padding': '0'
                })
                .append(
                    $('<li/>')
                        .css('display', 'block')
                        .text(errorMessage)
                )

            return bootstrap_modal;
        },
        clearErrorBlock: function () {
            error_block.find('ul').html('');

            return bootstrap_modal;
        },
        hideErrorBlock: function () {
            error_block.hide();

            return bootstrap_modal;
        },





        setSize: function (size) {
            var class_name = '';
            if (size == 'lg') {
                dialog.find('.modal-dialog').addClass('modal-lg');
            }
            if (size == 'sm') {
                dialog.find('.modal-dialog').addClass('modal-sm');
            }

            return bootstrap_modal;

        },
        clearTitle: function () {
            dialog.find('.js-modal-title').html('');
            return bootstrap_modal;
        },
        setTitle: function (title) {
            error_block.find('h3').html(title + ' error');
            dialog.find('.modal-title').html(title);
            return bootstrap_modal;
        },
        setContent: function (content) {
            dialog.find('.main-content').html(content);
            return bootstrap_modal;
        },
        show: function (staticModal) {
            if (staticModal) {
                dialog.attr('data-backdrop', 'static').attr('data-keyboard', 'false');
            }
            dialog.modal('show');
            return bootstrap_modal;
        },
        hide: function () {
            dialog.modal('hide');
        },
        shake: function () {
            dialog.find('.modal-content').addClass('shake');
            setTimeout(function () {
                dialog.find('.modal-content').removeClass('shake');
            }, 300)
        }
    };
})(jQuery);