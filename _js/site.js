$(function () {
    $('.js-add-card').on('click', function (e) {
        e.preventDefault();

        // Для начала посылаем POST запрос на получение списка всех артистов
        $.post('/get-artist-list', function (artist_array) {
            var content = $('<form/>').attr('method', 'post').attr('enctype', 'multipart/form-data')

            // Select для выбора года выпуска альбома
            var year = $('<select/>').attr({'name': 'year', 'class': 'form-control'});
            var year_start;
            for (year_start = 2019; year_start > 1900; year_start--) {
                year.append(
                    $('<option/>').attr('value', year_start).text(year_start)
                )
            }

            // Select для выбора длительности (Часов)
            var duration_hour = $('<select/>').attr({'name': 'duration_hour', 'class': 'form-control'});
            duration_hour.append($('<option/>').attr({'value': '-1', 'disabled': 'disabled'}).text('-- Часов --'))
            var duration_hour_start;
            for (duration_hour_start = 0; duration_hour_start < 30; duration_hour_start++) {
                duration_hour.append(
                    $('<option/>').attr('value', duration_hour_start).text(duration_hour_start)
                )
            }

            // Select для выбора длительности (Минут)
            var duration_minute = $('<select/>').attr({'name': 'duration_minute', 'class': 'form-control'});
            duration_minute.append($('<option/>').attr({'value': '-1', 'disabled': 'disabled'}).text('-- Минут --'))
            var duration_minute_start;
            for (duration_minute_start = 0; duration_minute_start < 59; duration_minute_start++) {
                var selected = false;
                if (duration_minute_start == 10) {
                    selected = 'selected';
                }
                duration_minute.append(
                    $('<option/>').attr('value', duration_minute_start).attr('selected', selected).text(duration_minute_start)
                )
            }


            // Select для выбора даты покупки (Год)
            var add_date_year = $('<select/>').attr({'name': 'add_date_year', 'class': 'form-control'});
            add_date_year.append($('<option/>').attr({'value': '-1', 'disabled': 'disabled'}).text('-- Год --'))
            var add_date_year_start;
            for (add_date_year_start = 2019; add_date_year_start > 1900; add_date_year_start--) {
                var selected = false;
                if (add_date_year_start == 2019) {
                    selected = 'selected';
                }
                add_date_year.append(
                    $('<option/>').attr('value', add_date_year_start).attr('selected', selected).text(add_date_year_start)
                )
            }

            // Select для выбора даты покупки (Месяц)
            var add_date_month = $('<select/>').attr({'name': 'add_date_month', 'class': 'form-control'});
            add_date_month.append($('<option/>').attr({'value': '-1', 'disabled': 'disabled'}).text('-- Месяц --'))
            var add_date_month_start;
            for (add_date_month_start = 1; add_date_month_start <= 12; add_date_month_start++) {
                var selected = false;
                if (add_date_month_start == 1) {
                    selected = 'selected';
                }
                add_date_month.append(
                    $('<option/>').attr('value', add_date_month_start).attr('selected', selected).text(add_date_month_start)
                )
            }

            // Select для выбора даты покупки (День)
            var add_date_day = $('<select/>').attr({'name': 'add_date_day', 'class': 'form-control'});
            add_date_day.append($('<option/>').attr({'value': '-1', 'disabled': 'disabled'}).text('-- День --'))
            var add_date_day_start;
            for (add_date_day_start = 1; add_date_day_start <= 31; add_date_day_start++) {
                var selected = false;
                if (add_date_day_start == 25) {
                    selected = 'selected';
                }
                add_date_day.append(
                    $('<option/>').attr('value', add_date_day_start).attr('selected', selected).text(add_date_day_start)
                )
            }

            // Загрузка обложка
            var image_preview = $('<img/>').css({'display': 'block', 'width': '100%'});
            var select_image_button =  $('<a/>').attr({'href': '#', 'class': 'btn btn-sm btn-outline-success btn-block'}).text('Загрузить').on('click', function (r) {
                r.preventDefault();

                input_image_file.click();
            })
            var input_image_file = $('<input/>').attr({'type': 'file', 'name': 'image'}).hide().on('change', function (e) {
                var input = this;

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        image_preview.attr('src', e.target.result);
                        select_image_button.text('Загрузить другую обложку')
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            })

            var add_new_artist = $('<input/>').attr({'name': 'artist_name', 'class': 'form-control', 'placeholder': 'Артист'})
            // Артист
            var select_artist = $('<select/>')
                .attr({'name': 'artist_id'})
                .addClass('form-control');

            select_artist
                .append(
                    $('<option/>').attr({'disabled': 'disabled', 'value': '-- Артист --'}).text('-- Артист --')
                )
                .append(
                    $('<option/>').attr({'value': '-1'}).text('-- Указать артиста вручную --')
                )
                .on('change', function (e) {
                    var val = $(this).val();
                    if (val == '-1') {
                        select_artist.hide();
                        add_new_artist.show();
                    }
                })

            if (artist_array) {
                $.each(artist_array, function (i, v) {
                    var selected = false;
                    if (i == 0) {
                        selected = 'selected';
                    }
                    select_artist.append(
                        $('<option/>').attr({'value': v.id, 'selected': selected}).text(v.name)
                    )
                })
                add_new_artist.hide();
            } else {
                select_artist.hide();
            }



            content
                .append(
                    $('<div/>').addClass('modal-body')
                        .append(
                            $('<div/>').addClass('alert alert-success').text('Для добавления карточки заполните все поля')
                        )
                        .append(
                            $('<div/>').addClass('row')
                                .append(
                                    $('<div/>').addClass('col-xl-12')
                                        .append(
                                            $('<label/>').css({'width': '100%'})
                                                .append('Название Альбома')
                                                .append(
                                                    $('<input/>').attr({'name': 'name', 'class': 'form-control'})
                                                )
                                        )
                                )
                                .append(
                                    $('<div/>').addClass('col-xl-6')
                                        .append(
                                            $('<label/>').css({'width': '100%'})
                                                .append('Артист')
                                                .append(
                                                    select_artist
                                                )
                                                .append(
                                                    add_new_artist
                                                )
                                        )
                                )
                                .append(
                                    $('<div/>').addClass('col-xl-3')
                                        .append(
                                            $('<label/>').css({'width': '100%'})
                                                .append('Год выпуска')
                                                .append(
                                                    year
                                                )

                                        )
                                )
                                .append(
                                    $('<div/>').addClass('col-xl-3')
                                        .append(
                                            $('<label/>').css({'width': '100%'})
                                                .append('Цена')
                                                .append(
                                                    $('<input/>').attr({'name': 'price', 'placeholder': '0.00', 'class': 'form-control'})
                                                )

                                        )
                                )
                                .append(
                                    $('<div/>').addClass('col-6')
                                        .append(
                                            $('<label/>').css({'width': '100%'})
                                                .append('Длительность')
                                                .append(
                                                    $('<div/>').addClass('row')
                                                        .append(
                                                            $('<div/>').addClass('col-6')
                                                                .append(duration_hour)
                                                        )
                                                        .append(
                                                            $('<div/>').addClass('col-6')
                                                                .append(duration_minute)
                                                        )
                                                )

                                        )
                                )
                                .append(
                                    $('<div/>').addClass('col-6')
                                        .append(
                                            $('<label/>').css({'width': '100%'})
                                                .append('Дата покупки')
                                                .append(
                                                    $('<div/>').addClass('row')
                                                        .append(
                                                            $('<div/>')
                                                                .addClass('col-4')
                                                                .append(add_date_year)
                                                        )
                                                        .append(
                                                            $('<div/>')
                                                                .addClass('col-4')
                                                                .append(add_date_month)
                                                        )
                                                        .append(
                                                            $('<div/>')
                                                                .addClass('col-4')
                                                                .append(add_date_day)
                                                        )


                                                )
                                        )
                                )
                                .append(
                                    $('<div/>').addClass('col-6')

                                        .append(
                                            $('<label/>').attr('for', 'code_room').text('Код хранилища')
                                        )
                                        .append(
                                            $('<div/>').addClass('row')
                                                .append(
                                                    $('<div/>')
                                                        .addClass('col-4')
                                                        .append(
                                                            $('<input/>').attr({'name': 'code_room', 'placeholder': 'Комната', 'class': 'form-control', 'id': 'code_room'})
                                                        )
                                                )
                                                .append(
                                                    $('<div/>')
                                                        .addClass('col-4')
                                                        .append(
                                                            $('<input/>').attr({'name': 'code_unit_box', 'placeholder': 'Стойка', 'class': 'form-control', 'id': 'code_room'})
                                                        )
                                                )
                                                .append(
                                                    $('<div/>')
                                                        .addClass('col-4')
                                                        .append(
                                                            $('<input/>').attr({'name': 'code_unit', 'placeholder': 'Место', 'class': 'form-control', 'id': 'code_room'})
                                                        )
                                                )

                                        )
                                )
                                .append(
                                    $('<div/>').addClass('col-6')
                                        .append(
                                            $('<label/>').css({'width': '100%'})
                                                .append('Обложка')
                                                .append(input_image_file)
                                                .append(select_image_button)

                                        )
                                        .append(image_preview)

                                )


                        )
                )
                .append(
                    $('<div/>').addClass('modal-footer')
                        .append(
                            $('<input/>').attr({'name': 'ok', 'type': 'submit', 'class': 'btn btn-sm btn-outline-success', 'value': 'Добавить'})
                        )
                )

            content.on('submit', function (e) {
                e.preventDefault();
                bootstrap_modal.hideErrorBlock();

                var $that = $(this),
                    formData = new FormData($that.get(0));

                $.ajax({
                    type: 'post',
                    url: '/card-add',
                    dataType: 'json',
                    contentType: false, // важно - убираем форматирование данных по умолчанию
                    processData: false, // важно - убираем преобразование строк по умолчанию
                    data: formData,
                    success: function(data) {
                        if (data.status == 'ok') {
                            document.location.reload();
                        } else {
                            $.each(data.errorArray, function (i, v) {
                                if (v == 'name') bootstrap_modal.addError('Укажите корректное название альбома')
                                if (v == 'artist') bootstrap_modal.addError('Вы не указали артиста')
                                if (v == 'duration') bootstrap_modal.addError('Продолжительность указана не корректно')
                                if (v == 'add_date_incorrect') bootstrap_modal.addError('Дата покупки указана не корректно')
                                if (v == 'price') bootstrap_modal.addError('Цена указана не корректно')
                                if (v == 'code') bootstrap_modal.addError('В хранилище это место уже занято')
                                bootstrap_modal.showErrorBlock();

                            })
                        }
                    }
                });
            })

            bootstrap_modal.setTitle('Добавление карточки').setContent(content).setSize('lg').show();
        }, 'json')
    })
});